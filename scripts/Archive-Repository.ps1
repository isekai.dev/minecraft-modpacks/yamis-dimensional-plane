<##
    .SYNOPSIS
    Archives the relevant files in the repository for importing into Twitch.

    .DESCRIPTION
    Archive-Repository will archive the `manifest.json` file and `overrides` folder (and all files contained within) into a single ZIP
    archive, ready for importing/uploading to CurseForge.

    Archive-Repository will replace an existing archive on disk, without prompting.

    .PARAMETER OutFile
    The name to be given to the resulting ZIP archive (excluding the extension). Default: Yami's Dimensional Plane

    .EXAMPLE
    Archive-Repository -OutFile 'yamis-dimensional-plane'
    yamis-dimensional-plane.zip
#>

Param(
    [Parameter(Mandatory = $False)]
    [string] $OutFile
)

## ----------- CONSTANTS ------------ ##

## Files and folder to archive.
$CONTENTS = @(
    './manifest.json'
    # './modlist.html'
    './overrides/'
)

## ---------- SCRIPT LOGIC ---------- ##

If ($OutFile -eq '') {
    ## No value provided for the parameter, so use default.
    $ArchiveName = "Yami's Dimensional Plane"
    $Version = Read-Host 'Version'

    If ($Version -eq '') {
        $OutFile = "$ArchiveName-gitlab-export"
    } Else {
        $OutFile = "$Archive-$Version"
    }
}

$Destination = ".\$OutFile.zip"

## Optimal Compression, for file uploads.
Compress-Archive -Path $CONTENTS -CompressionLevel Optimal -DestinationPath $Destination -Force
