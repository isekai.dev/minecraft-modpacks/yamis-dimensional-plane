<##
    .SYNOPSIS
    Extracts an archive downloaded/exported from CurseForge into the repository.

    .DESCRIPTION
    Extract-Archive will extract all files (except the `modlist.html` file) within the given ZIP archive, ready for committing to the
    repository.

    Extract-Archive will replace all existing files on disk with those in the archive, without prompting.

    .PARAMETER Path
    The path to the ZIP archive to extract. Required.

    .EXAMPLE
    Extract-Archive -Path "$HOME/dev/git/yamis-dimensional-plane/releases/Yami's Dimensional Plane-3.0.0-beta1.zip"
    overrides
    manifest.json
#>

Param(
    [Parameter(Mandatory = $True)]
    [string] $Path
)

## ---------- SCRIPT LOGIC ---------- ##

Expand-Archive -Path $Path -DestinationPath '.\' -Force

## `modlist.html` does not need to be version-controlled, nor is it required for importing/uploading into CurseForge.
Remove-Item -Path '.\modlist.html'
