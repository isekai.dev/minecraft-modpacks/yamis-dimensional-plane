# Yami's Dimensional Plane <!-- omit in toc -->

- [Key features](#key-features)
- [Tips / Tricks](#tips--tricks)
- [Example Dimension Stack](#example-dimension-stack)

## Key features

- Dimensional stacks! For example, a stack of the Overworld, 2 Skylands dimensions, a void world, and The End; simply travel vertically to travel between them (1000 blocks to get to The End!) - see command list below to configure this stack.
- Even mix of magic and tech with a couple mods of each focus: Create, Mekanism, Botania, Psi
- World pollution
- Nether spreading into the overworld
- Double-jumping and wall climbing

## Tips / Tricks

- Don't build a Nether Portal in your base (unless you want a Nether base!)
- Be careful not to dig through the obsidian "bedrock" at the bottom of the world

## Example Dimension Stack

Execute all of the below commands to setup the dimension stack.

```
/portal global connect_ceil minecraft:overworld immersive_portals:alternate1
/portal global connect_ceil immersive_portals:alternate1 immersive_portals:alternate2
/portal global connect_ceil immersive_portals:alternate2 immersive_portals:alternate5
/portal global connect_ceil immersive_portals:alternate5 minecraft:the_end
/portal global connect_floor minecraft:the_end immersive_portals:alternate5
/portal global connect_floor immersive_portals:alternate5 immersive_portals:alternate2
/portal global connect_floor immersive_portals:alternate2 immersive_portals:alternate1
/portal global connect_floor immersive_portals:alternate1 minecraft:overworld
```
