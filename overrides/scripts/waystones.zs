// Disable recipes for waystones
craftingTable.removeRecipe(<item:waystones:waystone>);
craftingTable.removeRecipe(<item:waystones:mossy_waystone>);
craftingTable.removeRecipe(<item:waystones:sandy_waystone>);

// Disable recipes for sharestones
craftingTable.removeRecipe(<item:waystones:sharestone>);
craftingTable.removeRecipe(<item:waystones:white_sharestone>);
craftingTable.removeRecipe(<item:waystones:orange_sharestone>);
craftingTable.removeRecipe(<item:waystones:magenta_sharestone>);
craftingTable.removeRecipe(<item:waystones:light_blue_sharestone>);
craftingTable.removeRecipe(<item:waystones:yellow_sharestone>);
craftingTable.removeRecipe(<item:waystones:lime_sharestone>);
craftingTable.removeRecipe(<item:waystones:pink_sharestone>);
craftingTable.removeRecipe(<item:waystones:gray_sharestone>);
craftingTable.removeRecipe(<item:waystones:light_gray_sharestone>);
craftingTable.removeRecipe(<item:waystones:cyan_sharestone>);
craftingTable.removeRecipe(<item:waystones:purple_sharestone>);
craftingTable.removeRecipe(<item:waystones:blue_sharestone>);
craftingTable.removeRecipe(<item:waystones:brown_sharestone>);
craftingTable.removeRecipe(<item:waystones:green_sharestone>);
craftingTable.removeRecipe(<item:waystones:red_sharestone>);
craftingTable.removeRecipe(<item:waystones:black_sharestone>);
