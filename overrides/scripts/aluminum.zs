// Add recipes for crushed aluminum ore
<recipetype:minecraft:smelting>.addRecipe(
    "smelt_crushed_aluminum", <item:tmechworks:aluminum_ingot>, <item:create:crushed_aluminum_ore>, 0.1, 200
);
<recipetype:minecraft:blasting>.addRecipe(
    "blast_crushed_aluminum", <item:tmechworks:aluminum_ingot>, <item:create:crushed_aluminum_ore>, 0.1, 100
);
<recipetype:create:splashing>.addRecipe(
    "wash_crushed_aluminum",
    [
        <item:tmechworks:aluminum_nugget> * 10,
        <item:tmechworks:aluminum_nugget> % 50,
        <item:tmechworks:aluminum_nugget> % 50,
        <item:tmechworks:aluminum_nugget> % 50,
        <item:tmechworks:aluminum_nugget> % 50,
        <item:tmechworks:aluminum_nugget> % 50
    ],
    <item:create:crushed_aluminum_ore>
);
