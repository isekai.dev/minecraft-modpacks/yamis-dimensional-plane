// Add recipe for Crushed Brass from Copper Dust
<recipetype:create:mixing>.removeRecipe(<item:create:crushed_brass> * 2);
<recipetype:create:mixing>.addRecipe("mix_crushed_brass", "heated", <item:create:crushed_brass> * 2, [
        <item:create:crushed_copper_ore> | <tag:items:forge:dusts/copper>,
        <item:create:crushed_zinc_ore>
]);
